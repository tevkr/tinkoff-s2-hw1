package dev.yanovsky.myproject

import zio.interop.catz._
import zio.interop.catz.implicits._

object Main extends scala.App {
  implicit val runtime: zio.Runtime[Any] = zio.Runtime.default
  val zioApp: zio.Task[Unit]             = MyprojectServer.run[zio.Task]
  runtime.unsafeRunSync(zioApp)
}
