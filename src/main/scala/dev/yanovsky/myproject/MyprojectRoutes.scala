package dev.yanovsky.myproject

import cats.effect.Async
import cats.implicits._
import dev.yanovsky.myproject.HelloWorld.Greeting
import org.http4s.HttpRoutes
import sttp.tapir._
import sttp.tapir.server.http4s.Http4sServerInterpreter

object MyprojectRoutes {
  val helloEndpoint = endpoint.get
    .in("hello")
    .errorOut(plainBody[String])
    .out(plainBody[Greeting])

  def helloWorldRoutes[F[_]: Async](H: HelloWorld[F]): HttpRoutes[F] =
    Http4sServerInterpreter[F]().toRoutes(
      helloEndpoint
        .serverLogic(_ =>
          H.hello()
            .attempt
            .map(_.leftMap(_.getMessage()))
        )
    )
}
