package dev.yanovsky.myproject

import cats.effect.Async
import cats.syntax.all._
import com.comcast.ip4s._
import org.http4s.ember.client.EmberClientBuilder
import org.http4s.ember.server.EmberServerBuilder
import org.http4s.implicits._
import org.http4s.server.middleware.Logger
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter

object MyprojectServer {
  def run[F[_]: Async]: F[Nothing] = {
    for {
      _            <- EmberClientBuilder.default[F].build
      helloWorldAlg = HelloWorld.impl[F]

      swagger = SwaggerInterpreter().fromEndpoints[F](
                  List(MyprojectRoutes.helloEndpoint),
                  "My project",
                  "1.0"
                )
      swaggerRoute = Http4sServerInterpreter[F]().toRoutes(swagger)

      httpApp = (
                  MyprojectRoutes.helloWorldRoutes[F](helloWorldAlg) <+>
                    swaggerRoute
                ).orNotFound

      finalHttpApp = Logger.httpApp(true, true)(httpApp)

      _ <-
        EmberServerBuilder
          .default[F]
          .withHost(ipv4"0.0.0.0")
          .withPort(port"8080")
          .withHttpApp(finalHttpApp)
          .build
    } yield ()
  }.useForever
}
