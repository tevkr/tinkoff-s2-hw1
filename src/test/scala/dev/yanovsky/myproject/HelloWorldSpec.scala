package dev.yanovsky.myproject

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class HelloWorldSpec extends AnyFlatSpec with Matchers {
  "hello method" should "return \"Hello World!\"" in {
    val helloWorld = HelloWorld.impl[cats.Id]
    val greeting   = helloWorld.hello().greeting
    greeting shouldBe "Hello World!"
  }
}
